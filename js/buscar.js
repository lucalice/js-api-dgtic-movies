$(document).ready(function(){

	var miLista = $("#miLista");
	var busqueda = $("#busqueda");

	$("#btn-buscar").on("click", function(){

		const palabra = $('#busqueda').val();
		const urlAPI = 'http://api.themoviedb.org/3/search/movie?certification_country=MX&language=es&api_key=1231231231564464541231&query="'+palabra+'"';
		console.log('Palabra a buscar: '+palabra);
		alert('Vamos a buscar: '+palabra);
		/*PONGA AQUI SU CÓDIGO*/
		// AJAX
		$.ajax({
			url:"http://api.themoviedb.org/3/search/movie?certification_country=MX&language=es&api_key=3356865d41894a2fa9bfa84b2b5f59bb&query=\""+palabra,
			success: function getMovies(respuesta){ //Respuesta es la variable que guarda el resultado de la consulta usando la API.
				$('#loader').remove();
				$('.listasPeliculasBuscadas').remove();

				console.log(respuesta);
				/*
					Con la propiedad results podemos acceder a la info de la API
				*/
				$.each(respuesta.results,function(index, elemento) { //Con la funcion $.each podemos ver cada resultado de la busqueda
					/*Vista Películas */
					if(elemento.poster_path != null){
						$(miLista).append(creaEstructura(elemento));
						console.log("Si se pudo");
					}
				});
			},
			error: function() {
				console.log("No se ha podido obtener la información");
			},
			beforeSend: function() { 
				//ANTES de hacer la petición se muestra la imagen de cargando.
				console.log('CARGANDO');
				$('#loader').html('<img class="mx-auto d-block" id="loader" src="images/loading.gif" />');
				console.log("Sali");
			},
		});
		/* Llamada a la API*/
		//http://api.themoviedb.org/3/search/movie?certification_country=MX&language=es&api_key=1231231231564464541231&query="palabra"
	});

});


function creaEstructura(elemento){
	return "<li class=\"listasPeliculasBuscadas\">"
				+'<div class="containerBusqueda">'
					+'<div class="card-header">'
						+'<img class="card-img" src="https://image.tmdb.org/t/p/w500/'+elemento.poster_path+'" alt="Card image">'
						+elemento.original_title
						+'<ul>'
							+'<li>'+'Popularidad: '+elemento.popularity+'</li>'
							+'<li>'+'Puntaje: '+elemento.vote_average+'</li>'
							+'<li>'+'Lenguaje Original: '+elemento.original_language+'</li>'
						+'</ul>'
					+'</div>'
				+'</div>'
			+'</li>';
}

