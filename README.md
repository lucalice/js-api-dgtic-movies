Carranza Escobar Luis Enrique

Mediante el uso de una API, vamos a consultar y realizar una consulta a la API
para que nos de la información de las películas dependiendo de la palabra que haya 
en el buscador.

Lo que yo hice para resolver el problema fue que declaré un segmento ajax,
dentro de ese segmento, me conecté a la API utilizando el link y la llave
que nos dieron. Una vez conectado, en la etiqueta de success, lo que hice fue
que por cada resultado que la API nos arroje, mandar a llamar a una función 
que nos creará el código HTML correspondiente para mostrar la información.
Además, modifiqué el archivo offcanvas.css para agregarle algunos estilos
a la información que aparece en el HTML. Utilicé HOOVER para que se vea un
poco más dinámica la selección de las películas.